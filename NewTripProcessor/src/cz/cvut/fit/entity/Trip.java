package cz.cvut.fit.entity;

import java.io.Serializable;

public class Trip implements Serializable {
	private String name;
    private int id;
    private int capacity;
    private int occupied;
 
    public Trip(int id, String name) {
        this.id = id;
        this.name = name;
    }
 
    public String getName() {
        return name;
    }
 
    public void setName(String name) {
        this.name = name;
    }
 
    public int getId() {
        return id;
    }
 
    public void setId(int id) {
        this.id = id;
    }

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public int getOccupied() {
		return occupied;
	}

	public void setOccupied(int occupied) {
		this.occupied = occupied;
	}
    
    
}