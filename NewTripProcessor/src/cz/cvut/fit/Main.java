package cz.cvut.fit;

import cz.cvut.fit.entity.Trip;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.Serializable;
import java.util.Hashtable;


public class Main implements MessageListener {

    private static final String newtriplQueueName = "jms/mdw-newtrips-queue";

    private static QueueConnection qcon;
    private static QueueSession qsession;
    private static QueueReceiver consumer;


    public static void main(String[] args) throws NamingException, JMSException {

        Main main = new Main();

        // create a JNDI context to lookup JNDI objects (connection factory and queue)
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, Config.JNDI_FACTORY);
        env.put(Context.PROVIDER_URL, Config.PROVIDER_URL);

        InitialContext ctx = new InitialContext(env);

        // create connection factory based on JNDI and a connection
        QueueConnectionFactory qconFactory = (QueueConnectionFactory) ctx.lookup(Config.JMS_FACTORY);
        qcon = qconFactory.createQueueConnection();

        // create a session within a connection
        qsession = qcon.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);

        // lookups the queue using the JNDI context
        Queue tripsQueue = (Queue) ctx.lookup(newtriplQueueName);

        consumer = qsession.createReceiver(tripsQueue);
        consumer.setMessageListener(main);

        qcon.start();

        try {
            synchronized (main) {
                while (true) {
                    main.wait();
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            close();
            System.out.println("Finished.");
        }

    }

    private static void close() throws JMSException {
        consumer.close();
        qsession.close();
        qcon.close();
    }

    @Override
    public void onMessage(Message message) {
        try {

            System.out.println("Message Received and processed: " + message.getJMSMessageID());
            Trip trip = (Trip) ((ObjectMessage)message).getObject();

            System.out.println("Trip id: " + trip.getId());
            System.out.println("Trip name: " + trip.getName());
            System.out.println("Trip capacity: " + trip.getCapacity());
            System.out.println("Trip occupied " + trip.getOccupied());
        } catch (JMSException jmse) {
            System.err.println("An exception occurred: " + jmse.getMessage());
        }
    }

}
