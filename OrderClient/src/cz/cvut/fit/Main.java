package cz.cvut.fit;
import cz.cvut.fit.entity.Booking;
import cz.cvut.fit.entity.Trip;

import java.io.*;
import java.util.Hashtable;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;


public class Main {

	static String generalQueueName = "jms/mdw-allorders-queue";

	public static void main(String[] args) {
		
		Trip trip = new Trip(1, "Vienna");
		trip.setCapacity(12);
		trip.setOccupied(2);
		
		Booking booking = new Booking();
		booking.setId(1);
		booking.setPerson("Mickey Mouse");
		booking.setTripId(1);


		try {
			send(generalQueueName, booking);
		} catch (NamingException | JMSException e) {
			e.printStackTrace();
		}
	}


    // sends the message to the queue
    public static void send(String queueName, Serializable obj) throws NamingException, JMSException {
        // create a JNDI context to lookup JNDI objects (connection factory and queue)
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, Config.JNDI_FACTORY);
        env.put(Context.PROVIDER_URL, Config.PROVIDER_URL);

        InitialContext ctx = new InitialContext(env);

        // create connection factory based on JNDI and a connection
        QueueConnectionFactory qconFactory = (QueueConnectionFactory) ctx.lookup(Config.JMS_FACTORY);
        QueueConnection qcon = qconFactory.createQueueConnection();
        
        // create a session within a connection
        QueueSession qsession = qcon.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);

        // lookups the queue using the JNDI context
        Queue queue = (Queue) ctx.lookup(queueName);

        // create sender and message
        QueueSender qsender = qsession.createSender(queue);

        // send the message and close
        try {
            Message msg = qsession.createObjectMessage(obj);

            if (obj instanceof Trip) {
                msg.setJMSType("Trip");
            } else if (obj instanceof Booking) {
                msg.setJMSType("Booking");
            }
//            producer.send(msg, DeliveryMode.NON_PERSISTENT, 8, 0);
            qsender.send(msg, DeliveryMode.PERSISTENT, 8, 0);
            System.out.println("The message " + msg.getJMSMessageID() + " was sent to the destination " +  qsender.getDestination().toString());
//            System.out.println("The message " + msg.getJMSMessageID() + " was sent to the destination " +  producer.getDestination().toString());
        } finally {
        	qsender.close();
            qsession.close();
            qcon.close();
        }
    }

}
