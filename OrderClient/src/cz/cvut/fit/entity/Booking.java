package cz.cvut.fit.entity;

import java.io.Serializable;

public class Booking implements Serializable {
	private int id;
	private int tripId;
	private String person;
	
	public Booking() {}

	public Booking(int id, int tripId, String person) {
		this.id = id;
		this.tripId = tripId;
		this.person = person;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getTripId() {
		return tripId;
	}
	
	public void setTripId(int tripId) {
		this.tripId = tripId;
	}
	
	public String getPerson() {
		return person;
	}
	
	public void setPerson(String person) {
		this.person = person;
	}
	
	
	
}
