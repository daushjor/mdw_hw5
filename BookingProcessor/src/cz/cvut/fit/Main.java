package cz.cvut.fit;

import cz.cvut.fit.entity.Booking;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.Serializable;
import java.util.Hashtable;


public class Main implements MessageListener {

    private static final String bookingQueueName = "jms/mdw-booking-queue";

    private static QueueConnection qcon;
    private static QueueSession qsession;
    private static QueueReceiver consumer;


    public static void main(String[] args) throws NamingException, JMSException {

        Main main = new Main();

        // create a JNDI context to lookup JNDI objects (connection factory and queue)
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, Config.JNDI_FACTORY);
        env.put(Context.PROVIDER_URL, Config.PROVIDER_URL);

        InitialContext ctx = new InitialContext(env);

        // create connection factory based on JNDI and a connection
        QueueConnectionFactory qconFactory = (QueueConnectionFactory) ctx.lookup(Config.JMS_FACTORY);
        qcon = qconFactory.createQueueConnection();

        // create a session within a connection
        qsession = qcon.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);

        // lookups the queue using the JNDI context

        Queue bookingQueue = (Queue) ctx.lookup(bookingQueueName);

        consumer = qsession.createReceiver(bookingQueue);
        consumer.setMessageListener(main);
        qcon.start();

        try {
            synchronized (main) {
                while (true) {
                    main.wait();
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            close();
            System.out.println("Finished.");
        }

    }

    private static void close() throws JMSException {
        consumer.close();
        qsession.close();
        qcon.close();
    }

    @Override
    public void onMessage(Message message) {
        try {
            System.out.println("Message Received and processed: " + message.getJMSMessageID());
            Booking booking = (Booking) ((ObjectMessage)message).getObject();

            System.out.println("Booking id: " + booking.getId());
            System.out.println("Booking tripId: " + booking.getTripId());
            System.out.println("Booking person: " + booking.getPerson());
        } catch (JMSException jmse) {
            System.err.println("An exception occurred: " + jmse.getMessage());
        }
    }

}
