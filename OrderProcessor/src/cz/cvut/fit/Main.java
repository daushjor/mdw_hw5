package cz.cvut.fit;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Hashtable;


public class Main {

    private static final String generalQueueName = "jms/mdw-allorders-queue";
    private static final String newtriplQueueName = "jms/mdw-newtrips-queue";
    private static final String bookingQueueName = "jms/mdw-booking-queue";

    private static QueueConnection qcon;
    private static QueueSession qsession;
    private static QueueSender tripProducer;
    private static QueueSender bookProducer;
    private static QueueReceiver receiver;
    private static Queue orderQueue;
    private static QueueConnectionFactory qconFactory;

    public static void main(String[] args) throws NamingException, JMSException {

        Main main = new Main();

        // create a JNDI context to lookup JNDI objects (connection factory and queue)
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, Config.JNDI_FACTORY);
        env.put(Context.PROVIDER_URL, Config.PROVIDER_URL);

        InitialContext ctx = new InitialContext(env);

        // create connection factory based on JNDI and a connection
        qconFactory = (QueueConnectionFactory) ctx.lookup(Config.JMS_FACTORY);
        qcon = qconFactory.createQueueConnection();

        // create a session within a connection
        qsession = qcon.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);

        orderQueue = (Queue) ctx.lookup(generalQueueName);
        Queue tripsQueue = (Queue) ctx.lookup(newtriplQueueName);
        Queue bookingQueue = (Queue) ctx.lookup(bookingQueueName);

        receiver = qsession.createReceiver(orderQueue);

        tripProducer = qsession.createSender(tripsQueue);
        bookProducer = qsession.createSender(bookingQueue);

        Listener listener = new Listener(bookProducer, tripProducer);

        receiver.setMessageListener(listener);
        qcon.start();
        main.listen();
    }

    private void listen() throws JMSException {

        try {
            synchronized (this) {
                while (true) {
                    this.wait();
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            close();
            System.out.println("Finished.");
        }
    }

    private void close() throws JMSException {
        receiver.close();
        tripProducer.close();
        bookProducer.close();
        qsession.close();
        qcon.close();
    }


}
