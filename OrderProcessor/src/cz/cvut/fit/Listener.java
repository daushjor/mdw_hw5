package cz.cvut.fit;

import javax.jms.*;
import javax.naming.NamingException;

/**
 * Created by George on 24-Nov-16.
 */
public class Listener implements MessageListener {

    private QueueSender bookProducer;
    private QueueSender tripProducer;

    public Listener (QueueSender bookProducer, QueueSender tripProducer) {
        this.bookProducer = bookProducer;
        this.tripProducer = tripProducer;
    }

    @Override
    public void onMessage(Message message) {
        try {
            System.out.println("Message Received: " + message.getJMSMessageID());
            if (message.getJMSType().equals("Trip")) {
                sendNewTrip(message);
            } else if (message.getJMSType().equals("Booking")) {
                sendBooking(message);
            }
        } catch (JMSException jmse) {
            System.err.println("An exception occurred: " + jmse.getMessage());
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    // sends the message to the queue
    public  void sendBooking(Message message) throws NamingException, JMSException {
        // send the message and close
        bookProducer.send(message);
        System.out.println("The message was sent to the BookingQueue");
    }

    // sends the message to the queue
    public  void sendNewTrip(Message message) throws NamingException, JMSException {
        // send the message and close
        tripProducer.send(message);
        System.out.println("The message was sent to the TripQueue");
    }
}
